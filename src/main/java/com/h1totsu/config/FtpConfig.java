package com.h1totsu.config;

import com.h1totsu.service.ftp.FtpService;
import com.h1totsu.service.ftp.FtpSettings;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FtpConfig
{
  @Value("${ftp.view.host}")
  private String ftpHost;

  @Value("${ftp.view.port}")
  private Integer ftpPort;

  @Value("${ftp.view.user}")
  private String ftpUser;

  @Value("${ftp.view.password}")
  private String ftpPassword;

  @Bean("viewFtpService")
  public FtpService createViewFtpService()
  {
    FtpService ftpViewService = new FtpService();
    ftpViewService.setFtpSettings(getFtpSettings());

    return ftpViewService;
  }

  private FtpSettings getFtpSettings()
  {
    FtpSettings ftpSettings = new FtpSettings();

    ftpSettings.setFtpHost(ftpHost);
    ftpSettings.setFtpPort(ftpPort);
    ftpSettings.setFtpUser(ftpUser);
    ftpSettings.setFtpPassword(ftpPassword);

    return ftpSettings;
  }
}
