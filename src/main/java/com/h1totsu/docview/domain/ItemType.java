package com.h1totsu.docview.domain;

public enum ItemType
{
  FOLDER, FILE
}
