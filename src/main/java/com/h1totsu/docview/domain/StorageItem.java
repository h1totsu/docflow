package com.h1totsu.docview.domain;

public class StorageItem
{
  private String name;
  private String path;
  private ItemType itemType;

  public StorageItem()
  {
  }

  public String getName()
  {
    return name;
  }

  public void setName(String name)
  {
    this.name = name;
  }

  public String getPath()
  {
    return path;
  }

  public void setPath(String path)
  {
    this.path = path;
  }

  public ItemType getItemType()
  {
    return itemType;
  }

  public void setItemType(ItemType itemType)
  {
    this.itemType = itemType;
  }

  @Override
  public String toString()
  {
    return "StorageItem{" +
        "name='" + name + '\'' +
        ", path='" + path + '\'' +
        ", itemType=" + itemType +
        '}';
  }
}
