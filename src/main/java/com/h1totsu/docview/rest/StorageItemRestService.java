package com.h1totsu.docview.rest;

import com.h1totsu.docview.domain.StorageItem;
import com.h1totsu.docview.process.FtpClientProcess;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class StorageItemRestService
{
  @Autowired
  FtpClientProcess ftpClientProcess;

  @RequestMapping(value = ViewUrlConstants.GET_ITEMS_URL, method = RequestMethod.GET)
  public List<StorageItem> getStorageItems(@RequestParam("path") String path)
  {
    return ftpClientProcess.getStorageItems(path);
  }

  @RequestMapping(value = ViewUrlConstants.GET_SUB_ITEMS_URL, method = RequestMethod.GET)
  public List<StorageItem> getStorageProgressItems(@RequestParam("path") String path)
  {
    return ftpClientProcess.getSubStorageItems(path);
  }
}
