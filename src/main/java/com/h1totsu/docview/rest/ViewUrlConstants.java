package com.h1totsu.docview.rest;

public interface ViewUrlConstants
{
  String URL_SEP = "/";

  String REST_URL = "rest";

  String GET_ITEMS_URL = URL_SEP + REST_URL + URL_SEP + "item";

  String GET_SUB_ITEMS_URL = GET_ITEMS_URL + URL_SEP + "sub";
}
