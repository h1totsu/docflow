package com.h1totsu.docview.converters;

import com.h1totsu.docview.domain.ItemType;
import com.h1totsu.docview.domain.StorageItem;
import org.apache.commons.net.ftp.FTPFile;

import java.io.File;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FtpItemConverter
{
  public static List<StorageItem> convertFiles(String workingDir, FTPFile... ftpFiles)
  {
    return Stream.of(ftpFiles)
        .map(ftpFile -> getStorageItem(workingDir, ftpFile))
        .collect(Collectors.toList());
  }

  private static StorageItem getStorageItem(String workingDir, FTPFile ftpFile)
  {
    StorageItem item = new StorageItem();
    item.setName(ftpFile.getName());
    item.setPath(workingDir + File.separator + ftpFile.getName());
    item.setItemType(ftpFile.isDirectory() ? ItemType.FOLDER : ItemType.FILE);

    return item;
  }

  public static List<StorageItem> convertPathToItems(String pathString)
  {
    List<StorageItem> storageItems = new ArrayList<>();
    Path path = FileSystems.getDefault().getPath(pathString);

    for (int i = 1; i < path.getNameCount(); i++)
    {
      Path subPath = path.subpath(0, i);
      StorageItem storageItem = new StorageItem();
      storageItem.setPath(File.separator + subPath.toString());
      storageItem.setName(subPath.getName(subPath.getNameCount() - 1).toString());
      storageItem.setItemType(ItemType.FOLDER);
      storageItems.add(storageItem);
    }

    return storageItems;
  }
}
