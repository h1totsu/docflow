package com.h1totsu.docview.process;

import com.h1totsu.docview.converters.FtpItemConverter;
import com.h1totsu.docview.domain.StorageItem;
import com.h1totsu.service.ftp.FtpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class FtpClientProcess
{
  @Autowired
  FtpService ftpViewService;

  public List<StorageItem> getStorageItems(String workingDir)
  {
    try
    {
      List<StorageItem> storageItems = FtpItemConverter
          .convertFiles(workingDir, ftpViewService.getFileList(workingDir));
      return getSortedListByType(storageItems);
    }
    catch (IOException e)
    {
      e.printStackTrace();
    }
    return null;
  }

  public List<StorageItem> getSortedListByType(List<StorageItem> storageItems)
  {
    return storageItems
        .stream()
        .sorted((storageItem, otherItem) -> storageItem.getItemType()
            .compareTo(otherItem.getItemType()))
        .collect(Collectors.toList());
  }

  public List<StorageItem> getSubStorageItems(String path)
  {
    return FtpItemConverter.convertPathToItems(path);
  }
}
