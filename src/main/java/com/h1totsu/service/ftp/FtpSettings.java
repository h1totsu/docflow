package com.h1totsu.service.ftp;

public class FtpSettings
{
  private String ftpHost;
  private Integer ftpPort;
  private String ftpUser;
  private String ftpPassword;

  public FtpSettings()
  {
  }

  public String getFtpHost()
  {
    return ftpHost;
  }

  public void setFtpHost(String ftpHost)
  {
    this.ftpHost = ftpHost;
  }

  public Integer getFtpPort()
  {
    return ftpPort;
  }

  public void setFtpPort(Integer ftpPort)
  {
    this.ftpPort = ftpPort;
  }

  public String getFtpUser()
  {
    return ftpUser;
  }

  public void setFtpUser(String ftpUser)
  {
    this.ftpUser = ftpUser;
  }

  public String getFtpPassword()
  {
    return ftpPassword;
  }

  public void setFtpPassword(String ftpPassword)
  {
    this.ftpPassword = ftpPassword;
  }
}
