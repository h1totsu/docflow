package com.h1totsu.service.ftp;

import org.apache.commons.net.ftp.FTPFile;
import org.apache.log4j.Logger;

import java.io.IOException;

public class FtpService
{
  private final Logger logger = Logger.getLogger(FtpService.class);

  private FtpSettings ftpSettings = new FtpSettings();

  public FtpService()
  {
  }

  public FTPFile[] getFileList(String workingDir) throws IOException
  {
    return new FtpEventHandler<FTPFile[]>()
        .handle((ftpClient) -> ftpClient.listFiles(workingDir), ftpSettings);
  }

  public FtpSettings getFtpSettings()
  {
    return ftpSettings;
  }

  public void setFtpSettings(FtpSettings ftpSettings)
  {
    this.ftpSettings = ftpSettings;
  }
}
