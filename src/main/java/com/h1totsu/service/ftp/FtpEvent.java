package com.h1totsu.service.ftp;

import org.apache.commons.net.ftp.FTPClient;

import java.io.IOException;

@FunctionalInterface
public interface FtpEvent
{
  Object execute(FTPClient ftpClient) throws IOException;
}
