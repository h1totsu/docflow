package com.h1totsu.service.ftp;

import org.apache.commons.net.ftp.FTPClient;

import java.io.IOException;

public class FtpEventHandler<T>
{
  private FTPClient ftpClient = new FTPClient();

  public T handle(FtpEvent ftpEvent, FtpSettings ftpSettings) throws IOException
  {
    ftpClient.setControlEncoding("UTF-8");
    ftpClient.connect(ftpSettings.getFtpHost(), ftpSettings.getFtpPort());
    ftpClient.login(ftpSettings.getFtpUser(), ftpSettings.getFtpPassword());
    ftpClient.enterLocalPassiveMode();

    T result = (T) ftpEvent.execute(ftpClient);
    try
    {
      if (ftpClient.isConnected())
      {
        ftpClient.logout();
        ftpClient.disconnect();
      }
    }
    catch (IOException e)
    {
      throw new RuntimeException(e);
    }
    return result;
  }
}
