package com.h1totsu.service.ftp;

import org.apache.commons.net.ftp.FTPFile;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileUtils
{
  public static List<String> getFilesNames(FTPFile... ftpFiles)
  {
    return Stream.of(ftpFiles)
        .map(FTPFile::getName)
        .collect(Collectors.toList());
  }
}
