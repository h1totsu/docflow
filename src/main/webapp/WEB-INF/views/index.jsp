<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Notes</title>
    <link rel="stylesheet"
          href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap.min.css"
          integrity="sha384-AysaV+vQoT3kOAXZkl02PThvDr8HYKPZhNT5h/CXfBThSRXQ6jW5DO2ekP5ViFdi"
          crossorigin="anonymous">
    <link rel="stylesheet" href="/styles/cover.css"/>
</head>
<body>
<div class="site-wrapper">

    <div class="site-wrapper-inner">

        <div class="cover-container">

            <div class="masthead clearfix">
                <div class="inner">
                    <h3 class="masthead-brand">Main</h3>
                </div>
            </div>

            <form style="margin: 0 250px" method="post" action="/j_spring_security_check">
                <c:if test="${param.error ne null}">
                    <div class="alert alert-danger" role="alert">
                        <strong>Invalid username and password.</strong>
                    </div>
                </c:if>
                <c:if test="${param.logout ne null}">
                    <div class="alert alert-info" role="alert">
                        <strong>You have been logged out.</strong>
                    </div>
                </c:if> 
                <div class="form-group">
                    <input class="form-control" type="text" name="login" placeholder="User Name"/> 
                </div>
                  
                <div class="form-group">
                    <input class="form-control" type="password" name="password"
                           placeholder="Password"/>
                </div>
                  
                <div class="form-group">
                    <button class="btn btn-info" type="submit">Sign In</button>
                </div>
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
            </form>

            <div class="mastfoot">
                <div class="inner">
                    <p>Created only for <a href="http://getbootstrap.com">H1totsu</a>, by <a
                            href="#">@h1totsu</a>.</p>
                </div>
            </div>

        </div>
    </div>
</div>
</body>
</html>
