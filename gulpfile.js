var gulp = require('gulp');
var bower = require('gulp-bower');
var watch = require('gulp-watch');
var mainBowerFiles = require('main-bower-files');

gulp.task('load-vendors', ['bower-install'], function () {
    return gulp.src(mainBowerFiles(), {base: 'bower_components'})
        .pipe(gulp.dest('src/main/resources/static/scripts/vendors'));
});


gulp.task('bower-install', function () {
    return bower();
});